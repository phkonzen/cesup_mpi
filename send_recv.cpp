#include <iostream>
#include <mpi.h>
#include <assert.h>

int main(int argc, char** argv) {
  
    // Inicializa o ambiente MPI
    MPI_Init(NULL, NULL);

    // Número de processos
    int nproc;
    MPI_Comm_size(MPI_COMM_WORLD, &nproc);

    // aborta caso nproc != 2
    if (nproc != 2) {
      int erro;
      MPI_Abort(MPI_COMM_WORLD, erro);
    }

    // posição (id) do processo
    int pid;
    MPI_Comm_rank(MPI_COMM_WORLD, &pid);

    // dados iniciais
    int a, b, c;

    // pid == 0
    if (pid == 0) {
      a = 2;
      
      // envia a para b no pid 1
      MPI_Send(&a, 1, MPI_INT, 1, 0, MPI_COMM_WORLD);
      // b recebe a do pid 1
      MPI_Recv(&b, 1, MPI_INT, 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

      // computa a+b
      c = a + b;
      std::cout << "pid " << pid << ": "
		<< a << " + " << b
		<< " = " << c
		<< std::endl;
    }
    else { //pid == 1
      a = 3;

      // b recebe a do pid 0
      MPI_Recv(&b, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      // envia a para b no pid 0
      MPI_Send(&a, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);

      // computa a-b
      c = a - b;
      std::cout << "pid " << pid << ": "
		<< a << " - " << b
		<< " = " << c
		<< std::endl;
    }

    // aguarda os dois processos
    MPI_Barrier(MPI_COMM_WORLD);
    
    if (pid == 0)
      std::cout << "Feito." << std::endl;
    
    // Finaliza o ambiente MPI
    MPI_Finalize();

    return 0;
}
