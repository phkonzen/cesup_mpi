# Introdução sobre implementação e compilação de códigos C++/MPI em cluster

* Pedro H A Konzen
* [http://professor.ufrgs.br/pedro](http://professor.ufrgs.br/pedro)

## Introdução

Este minicurso busca ser uma rápida introdução sobre implementação, compilação e execução de códigos C++/MPI no cluster `GAUSS` do [CESUP-UFRGS][CESUP]. O objetivo é de inserir a(o) participante neste contexto pela apresentação e discussão de exemplos de aplicações simples.

### MPI

MPI (_Message Passing Interface_) é uma padrão de troca de mensagens de programação paralela. Há vários tutoriais sobre MPI disponíveis na internet, por exemplo, veja [MPI Tutorial](https://mpitutorial.com/).

### Cluster `GAUSS`

O cluster `GAUSS` é um dos recursos disponibilizados pelo [CESUP-UFRGS][CESUP] a seus usuários. Opera com o sistema Novell SUSE Linux Enterprise Server 11-SP1 e conta com uma unidade de conexão e 64 unidades de processamento, cada uma com 64GB de RAM e 2 processadores dodecacore AMD Opteron, totalizando 1536 núcleos de processamento. Veja mais sobre o cluster no [Guia do Usuário][GuiaUsuario].

## Primeiros passos

### Acesso ao cluster

O acesso remoto a `GAUSS` pode ser feito com a linha de comando:

	$ ssh usuario@gauss.cesup.ufrgs.br
	
onde, `usuario` dese ser substituído pelo nome de usuário da conta disponibilizada pelo CESUP.

### Acesso ao material

O material do curso está disponível no repositório [GitLab][GitRepo]. Para clonar o repositório, digite

	$ git clone git@gitlab.com:phkonzen/cesup_mpi.git

Com isso, a pasta `cesup_mpi` com o material deste curso terá sido criada. Para entrar na pasta, digite:

	$ cd cesup_mpi

Então, para listar os arquivos da pasta, digite:

	$ ls
	
## Olá, MPI!

### Implementação

Veja a implementação C++/MPI da primeira aplicação `ola.cpp`. A `GAUSS` disponibiliza alguns editores de texto, como o `emacs`, o `nano` e o `vi`. Para ler o código com o `vi`, digite:

	$ vi ola.cpp
	
### Compilação
	
Para compilar o código, digite:

	$ g++ ola.cpp -lmpi -lmpi++
	
Com isso, o arquivo executável `a.out` terá sido criado.

### Execução

A execução de aplicações no cluster `GAUSS` deve ser feita na pasta `$DADOS` do usuário. Para entrar na pasta, digite:

	$ cd $DADOS
	
A solicitação de execução/processamento deve ser feita pelo sistema de filas `PBS`. Para isso, deve-se editar um _script_ PBS com as informações sobre o processamento desejado. 

No caso da aplicação ola.cpp, pode-se utilizar o arquivo `script.sh` disponível na pasta do curso. Para tanto, copie este arquivo para a pasta `$DADOS`:

	$ cp $HOME/cesup_mpi/script.sh .

Então, para colocar a execução em fila, digite:

	$ qsub script.sh

Para ver a posição da solicitação na fila, digite:

	$ qstat
	
Para ver o _status_ das suas solicitações, digite:

	$ qstat -u usuario
	
Finalizada a execução, um arquivo com o _output_ da aplicação será criado na pasta `$DADOS`. Caso a execução tenha ocorrido com sucesso, o arquivo terá nome `ola.oN`, onde `N` é o número do processo na fila. Caso tenha havido erro na execução, o arquivo terá o nome `ola.eN`.

Tendo executado com sucesso, abra o arquivo de _output_, digitando:

	$ vi ola.oN
	
lembrando de substituir o `N` pelo número do processo na fila.

## Envio e recebimento de dados por MPI

Existem várias funções MPI para realizar a comunicação de dados entre os processos de uma aplicação. As funções fundamentais são

	MPI_Send(
		void* data,
		int count,
		MPI_Datatype datatype,
		int destination,
		int tag,
		MPI_Comm communicator)

e

	MPI_Recv(
		void* data,
		int count,
		MPI_Datatype datatype,
		int source,
		int tag,
		MPI_Comm communicator,
		MPI_Status* status)

Veja a aplicação `send_recv.cpp`.

## Comunicação coletiva

O MPI conta com funções de comunicação coletiva para otimizar o envio e recebimento de dados entre grupos de processos. Algumas destas funções são `MPI_Bcast`, `MPI_Scatter` e o `MPI_Gather`.

Além destas, há a funções de redução, as quais permitem a computação de operações com os dados que sejam enviados e recebidos entre os processos. Aqui, vamos usar a função:

	MPI_Reduce(
		void* send_data,
		void* recv_data,
		int count,
		MPI_Datatype datatype,
		MPI_Op op,
		int root,
		MPI_Comm communicator)
	
O código `reduce.cpp` computa a aproximação da $\int e^{-x^2} dx$ pela regra do trapézio composta.

## Para o além

### Tutoriais introdutórios sobre MPI

* MPI Tutorial, Author Wes Kendall: [https://mpitutorial.com/](https://mpitutorial.com/)

* Message Passing Interface (MPI), Author: Blaise Barney: [https://computing.llnl.gov/tutorials/mpi/](https://computing.llnl.gov/tutorials/mpi/)

### Protocolos MPI disponíveis na `GAUSS`

Para escolher e ver os protocolos MPI disponíveis na `GAUSS`, digite:

	$ mpi-selector-menu

[CESUP]: http://www.cesup.ufrgs.br/
[GuiaUsuario]: http://arquivos.cesup.ufrgs.br/manuais/gauss.pdf
[GitRepo]: https://gitlab.com/phkonzen/cesup_mpi

    

