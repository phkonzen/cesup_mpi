#include <iostream>
#include <mpi.h>

#include <math.h>

double f(double x)
{
  return exp(-x*x);
}

int main(int argc, char** argv) {
  
    // Inicializa o ambiente MPI
    MPI_Init(NULL, NULL);

    // Número de processos
    int nproc;
    MPI_Comm_size(MPI_COMM_WORLD, &nproc);

    // posição (id) do processo
    int pid;
    MPI_Comm_rank(MPI_COMM_WORLD, &pid);

    // num. pontos
    int n = 1000;
    double h = 4.0/(n-1);

    int ni = 1 + pid*(n-2)/nproc;
    int nf = (pid+1)*(n-2)/nproc;

    double s=0;
    for (int i=ni; i<= nf; i++)
      s += f(-2 + i*h);

    std::cout << "pid " << pid
	      << " feito." << std::endl;

    double sr;
    MPI_Reduce(&s, &sr, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

    if (pid == 0) {
      s = h*sr + h/2*(f(-2) + f(2));
      
      std::cout << "s = "
		<< s
		<< std::endl;
    }
    
    // Finaliza o ambiente MPI
    MPI_Finalize();

    return 0;
}
