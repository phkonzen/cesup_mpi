#include <iostream>
#include <mpi.h>

int main(int argc, char** argv) {
  
    // Inicializa o ambiente MPI
    MPI_Init(NULL, NULL);

    // Número de processos
    int nproc;
    MPI_Comm_size(MPI_COMM_WORLD, &nproc);

    // posição (id) do processo
    int pid;
    MPI_Comm_rank(MPI_COMM_WORLD, &pid);

    // Nome do processador
    char pnome[MPI_MAX_PROCESSOR_NAME];
    int pnome_t;
    MPI_Get_processor_name(pnome, &pnome_t);

    // Escreve mensagem
    std::cout << "Olá, diz o processador "
	      << pnome
	      << " (id: "
	      << pid
	      << ")." << std::endl;

    // Finaliza o ambiente MPI
    MPI_Finalize();

    return 0;
}
